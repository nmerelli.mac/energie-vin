# frozen_string_literal: true

# This seed is only for demonstration purposes.

## Wine ##
current_year = Time.current.year
wine_colors = Wine.colors.values
wines_attributes = 100.times.map do
  {
    name: Faker::Lorem.unique.sentence.chop, # No faker factory for wines sadly
    year: Faker::Number.within(range: 1990..current_year),
    color: wine_colors.sample
  }
end

wines = Wine.insert_all!(wines_attributes, returning: %i[id year]).to_a
puts "Inserted #{wines.count} wines."

## Merchant ##

merchant_attributes = 25.times.map do |time|
  {
    name: Faker::Company.name,
    description: time.even? ? Faker::Company.bs : nil,
    website: Faker::Internet.url
  }
end

merchants = Merchant.insert_all!(merchant_attributes).to_a
puts "Inserted #{merchants.count} merchants."

## Wine::Reference ##

wine_references_attributes = 300.times.map do
  wine = wines.sample
  merchant = merchants.sample

  {
    wine_id: wine['id'],
    merchant_id: merchant['id'],
    price: Faker::Number.within(range: 0.0..2000.0).round(2),
    url: Faker::Internet.url
  }
end

wine_references = Wine::Reference.insert_all!(wine_references_attributes).to_a
puts "Inserted #{wine_references.count} wine references."

## Wine::CriticRating ##
wine_critic_ratings_attributes = 300.times.map do |time|
  wine = wines.sample

  {
    wine_id: wine['id'],
    author: Faker::Name.name,
    rating: Faker::Number.within(range: 0..100),
    review: time.even? ? Faker::Lorem.paragraph : nil,
    url: Faker::Internet.url,
    tasted_at: Faker::Time.between(from: Date.new(wine['year']), to: Time.current)
  }
end

wine_critic_ratings = Wine::CriticRating.insert_all!(wine_critic_ratings_attributes).to_a
puts "Inserted #{wine_critic_ratings.count} wine critic ratings."
