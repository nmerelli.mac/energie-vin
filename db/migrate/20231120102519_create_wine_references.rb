class CreateWineReferences < ActiveRecord::Migration[7.1]
  def change
    create_table :wine_references do |t|
      t.references :wine, foreign_key: true, null: false
      t.references :merchant, foreign_key: true, null: false
      t.float :price, null: false
      t.string :url, null: false
      t.timestamps
    end
  end
end
