class CreateWineCriticRatings < ActiveRecord::Migration[7.1]
  def change
    create_table :wine_critic_ratings do |t|
      t.references :wine, foreign_key: true, null: false
      t.string :author, null: false
      t.integer :rating, null: false
      t.text :review
      t.string :url, null: false
      t.datetime :tasted_at, null: false
      t.timestamps
    end
  end
end
