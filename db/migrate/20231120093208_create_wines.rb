class CreateWines < ActiveRecord::Migration[7.1]
  def change
    create_table :wines do |t|
      t.string :name, null: false
      t.integer :year, null: false
      t.string :color, null: false
      t.timestamps

      t.index %i[name year], name: :index_wines_unique, unique: true
    end
  end
end
