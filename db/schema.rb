# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.1].define(version: 2023_11_20_102519) do
  create_table "merchants", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.string "website", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wine_critic_ratings", force: :cascade do |t|
    t.integer "wine_id", null: false
    t.string "author", null: false
    t.integer "rating", null: false
    t.text "review"
    t.string "url", null: false
    t.datetime "tasted_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["wine_id"], name: "index_wine_critic_ratings_on_wine_id"
  end

  create_table "wine_references", force: :cascade do |t|
    t.integer "wine_id", null: false
    t.integer "merchant_id", null: false
    t.float "price", null: false
    t.string "url", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["merchant_id"], name: "index_wine_references_on_merchant_id"
    t.index ["wine_id"], name: "index_wine_references_on_wine_id"
  end

  create_table "wines", force: :cascade do |t|
    t.string "name", null: false
    t.integer "year", null: false
    t.string "color", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "year"], name: "index_wines_unique", unique: true
  end

  add_foreign_key "wine_critic_ratings", "wines"
  add_foreign_key "wine_references", "merchants"
  add_foreign_key "wine_references", "wines"
end
