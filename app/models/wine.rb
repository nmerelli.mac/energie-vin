# frozen_string_literal: true

class Wine < ApplicationRecord
  YEAR_GTEQ = 1900

  has_many :critic_ratings, -> { order(rating: :desc) }, class_name: 'Wine::CriticRating'
  has_many :references, -> { order(price: :asc) }, class_name: 'Wine::Reference'

  enum color: { red: 'red', white: 'white', rose: 'rose' }

  validates :name, :year, :color, presence: true
  validates :name, uniqueness: { scope: %i[year] }
  validates :year, numericality: { greater_than_or_equal_to: YEAR_GTEQ, less_than_or_equal_to: Date.current.year }

  class << self
    # Return the wines with at least one merchant reference, sorted by the average of critic ratings
    # Can search by minimum/maximum price relative to the minimum price of the references of each wine
    def search(price_min:, price_max:)
      query = Wine.select('wines.id, wines.name, wines.year',
                          'COUNT(wine_critic_ratings.rating) as critic_ratings_count',
                          'AVG(wine_critic_ratings.rating) as critic_ratings_average',
                          'MIN(wine_references.price) as references_min_price')
                  .joins(:references)
                  .left_joins(:critic_ratings)
      query = query.having('references_min_price >= ?', price_min.to_f) if price_min.present?
      query = query.having('references_min_price <= ?', price_max.to_f) if price_max.present?
      query.group('wines.id, wines.name, wines.year')
           .order('critic_ratings_average desc')
    end
  end
end
