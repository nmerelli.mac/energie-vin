class Merchant < ApplicationRecord
  has_many :wine_references, class_name: 'Wine::Reference'

  validates :name, :website, presence: true
end
