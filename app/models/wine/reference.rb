class Wine
  class Reference < ApplicationRecord
    belongs_to :wine
    belongs_to :merchant

    validates :wine, :merchant, :price, :url, presence: true
  end
end
