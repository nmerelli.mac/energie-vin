class Wine
  class CriticRating < ApplicationRecord
    SCORE_GTEQ = 0
    SCORE_LTEQ = 100

    belongs_to :wine

    validates :wine, :author, :rating, :url, :tasted_at, presence: true
    validates :score, numericality: {
      greater_than_or_equal_to: SCORE_GTEQ,
      less_than_or_equal_to: SCORE_LTEQ
    }
  end
end
