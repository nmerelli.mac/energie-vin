class MerchantsController < ApplicationController
  def show
    @merchant = merchant
  end

  private

  def merchant
    @merchant ||= Merchant.find(params[:id])
  end
end
