# frozen_string_literal: true

class WinesController < ApplicationController
  def index
    wines = Wine.search(price_min: index_params[:price_min], price_max: index_params[:price_max])
    @wines = Kaminari.paginate_array(wines).page(params[:page])
    @params = index_params
  end

  def show
    @wine = wine
  end

  private

  def wine
    @wine ||= Wine.includes([{ references: [:merchant] }, :critic_ratings]).find(params[:id])
  end

  def index_params
    params.permit(:price_min, :price_max)
  end
end
