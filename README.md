# README

This README would normally document whatever steps are necessary to get the
application up and running.

## Ruby version

The ruby-version of the project is 3.2.2.

## Installation

```bash
bundle install
```

## Database creation

SQLite is used in this project, no need for postgres

```bash
rails db:drop db:create db:migrate
```

DB Schema:

![Database](doc/db.png)

## Database initialization

Seeds are provided for testing

```bash
rails db:seed
```

## How to run the test suite

No tests have been provided sadly, too much time constraints

## Launch server

Launch server with 
```bash
rails s
```

The server will be listing on http://127.0.0.1:3000

Happy testing !

